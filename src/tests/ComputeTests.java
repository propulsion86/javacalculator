package tests;


import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import source.Compute;

import java.util.List;

public class ComputeTests {

    @Test
    void simpleAritmetic(){ //tests the addition, subtraction, division, multiplication, power, decimal, and E functionality of the compute class.
        List<String> temp = List.of("2", "3", "+", "2", "4");
        Double answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 47.0);

        temp = List.of("3", "4", "-", "6");
        answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 28.0);

        temp = List.of("3", "4", "/", "4");
        answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 8.5);

        temp = List.of("3", "4", "*", "4");
        answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 136.0);

        temp = List.of("3", "4", "^", "4");
        answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 1336336.0);

        temp = List.of("3", "4", ".", "4", "+", "5");
        answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 39.4);

        temp = List.of("3", "4", ".", "4", "E", "5");
        answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 3440000.0);
    }

    @Test
    void bedmasTest(){ // tests wether the compute classes computations follow bedmas.
        List<String> temp = List.of("2", "^", "3", "-", "(", "5", "*", "5", ")", "+", "2", "E", "3");
        Double answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 1983.0);

        temp = List.of("(", "(", "6", "-", "2", ")", "*", "2", ")", "^", "2");
        answer = Compute.evaluateEquation(temp);
        assertEquals(answer, 64.0);
    }

}
