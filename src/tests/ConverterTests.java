package tests;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import source.Converter;

public class ConverterTests {
    @Test
    void intToStringTest(){
        for(int i=0; i<10; i++){
            assertEquals(Converter.intToString(i), String.valueOf(i));
        }
        assertEquals(Converter.intToString(10), "+");
        assertEquals(Converter.intToString(11), "-");
        assertEquals(Converter.intToString(12), "/");
        assertEquals(Converter.intToString(13), "*");
        assertEquals(Converter.intToString(14), "^");
        assertEquals(Converter.intToString(15), "(");
        assertEquals(Converter.intToString(16), ")");
        assertEquals(Converter.intToString(17), ".");
        assertEquals(Converter.intToString(18), "E");
        assertEquals(Converter.intToString(19), "DEL");
        assertEquals(Converter.intToString(20), "EXEC");
        assertEquals(Converter.intToString(21), "AC");
    }

    @Test
    void stringToIntTest(){
        assertEquals(Converter.stringToInt("0"), 0);
        assertEquals(Converter.stringToInt("1"), 1);
        assertEquals(Converter.stringToInt("2"), 2);
        assertEquals(Converter.stringToInt("3"), 3);
        assertEquals(Converter.stringToInt("4"), 4);
        assertEquals(Converter.stringToInt("5"), 5);
        assertEquals(Converter.stringToInt("6"), 6);
        assertEquals(Converter.stringToInt("7"), 7);
        assertEquals(Converter.stringToInt("8"), 8);
        assertEquals(Converter.stringToInt("9"), 9);
        assertEquals(Converter.stringToInt("+"), 10);
        assertEquals(Converter.stringToInt("-"), 11);
        assertEquals(Converter.stringToInt("/"), 12);
        assertEquals(Converter.stringToInt("*"), 13);
        assertEquals(Converter.stringToInt("^"), 14);
        assertEquals(Converter.stringToInt("("), 15);
        assertEquals(Converter.stringToInt(")"), 16);
        assertEquals(Converter.stringToInt("."), 17);
        assertEquals(Converter.stringToInt("E"), 18);
        assertEquals(Converter.stringToInt("DEL"), 19);
        assertEquals(Converter.stringToInt("EXEC"), 20);
        assertEquals(Converter.stringToInt("AC"), 21);
    }

}
