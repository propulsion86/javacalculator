package source;
import javax.swing.*;
import java.awt.*;

public class UserInterface extends JFrame {
    private outputPanel out; //the output panel
    private inputPanel in; //the input panel
    private CommonData comm; //the common data shared between input and output.

    /**
     * Construtor makes the interface.
     */
    public UserInterface(){
        comm = new CommonData(); //make the common data class.
        this.setBounds(20, 20, 800, 800); //set bounds of the frame, set the backround color and the layout.
        this.setBackground(new Color(40, 40, 40));
        this.setLayout(new GridLayout(2, 1));
        out = new outputPanel(comm); //make and add the input and output panels.
        this.add(out);
        in = new inputPanel(comm);
        this.add(in);

        this.setVisible(true); //make the frame visible.
    }
}