package source;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class inputPanel extends JPanel {
    private List<JButton> buttons = new ArrayList<>(); // list of all the buttons.

    private CommonData data; //allows access to the common data shared between input, output and computes.

    /**
     * constructor
     * @param c the CommonData
     */
    public inputPanel(CommonData c){
        data = c; //set the common data.
        this.setLayout(new GridLayout(3, 10)); //make the button grid.
        this.setBackground(new Color(40, 40, 40)); //set the background color
        this.setOpaque(true);
        setupButtons(); //call the setup buttons method.
    }

    /**
     * sets up all the buttons and their specific functions that happen when clicked.
     */
    private void setupButtons(){
        for(int i=0; i < 22; i++){ //for all 21 buttons.
            String text = Converter.intToString(i); //get the string associated with the number.


            JButton temp = new JButton(text); //set the text, background, foreground and font of the button.
            temp.setBackground(new Color(40, 40, 40));
            temp.setForeground(new Color(235,219, 178));
            temp.setFont(new Font("Sans", Font.PLAIN, 20));
            if(i == 19){ // if delete button make the button remove the last string added, set the update varible.
                temp.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        data.delInputEnd();
                        data.setUpdate();
                    }
                });
            }else if(i == 20){ //If exec button then get the answer then set the answer as the input so that it shows up on the display then set the update variable.
                temp.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Double ans = Compute.evaluateEquation(data.getInput());
                        String ansStr = ans.toString();
                        data.clearInput();
                        for(char c : ansStr.toCharArray()){
                            data.addInput(String.valueOf(c));
                        }
                        data.setUpdate();

                    }
                });
            }else if(i==21){ // If AC button remove all text from input then set update varible.
                temp.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        data.clearInput();
                        data.setUpdate();

                    }
                });

            }else{ //for all other buttons just make then add their text to the input and set the update variable.
                int finalI = i;
                temp.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        data.addInput(text);
                        System.out.println(finalI + " " + Converter.intToString(finalI));
                        data.setUpdate();
                    }
                });
            }

            buttons.add(temp); //add the button to the list and the JPanel.
            this.add(temp);
        }
    }

}
