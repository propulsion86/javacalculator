package source;
import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class outputPanel extends JPanel {
    JTextArea output = new JTextArea(); //the JTextArea that shows the output.
    private ExecutorService exec; //the executor service that runs the updater.
    private Runnable updater; //the runnable that checks for updates.
    private CommonData data; //the commondata shared between the input and output classes.

    /**
     * constructor sets up the output and makes runnable.
     * @param c the CommonData.
     */
    public outputPanel(CommonData c){
        data = c; //set the common data
        this.setBackground(new Color(40, 40, 40)); //set background, opacity, font, text , foreground, bounds and visibility of the text area and panel.
        this.setOpaque(true);
        output.setFont(new Font("Sans", Font.PLAIN, 40));
        output.setText(data.getToOut());
        output.setBackground(new Color(40, 40, 40));
        output.setForeground(new Color(235,219, 178));
        output.setBounds(0, 0, 800, 400);
        output.setVisible(true);
        this.add(output);
        exec = Executors.newSingleThreadExecutor(); //make the executor service
        updater = () -> { //the updater checks for the update variable in the common data. if set it will update the JTextArea. Otherwise, it will wait for 10ms.
            while(true){
                if(data.getUpdate()){
                    output.setText(data.getToOut());
                    data.unSetUpdate();
                }else{
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };

        exec.execute(updater);
    }




}
