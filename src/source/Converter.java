package source;
import java.util.ArrayList;
import java.util.List;

public class Converter {
    public Converter(){
        
    }

    /**
     * used for setting up the buttons
     * @param i the number button
     * @return the text it should have in it
     */
    public static String intToString(Integer i){ //used for setting up the buttons.
        if(i<10){
            return String.valueOf(i);
        }else if(i==10){
            return "+";
        }else if(i==11){
            return "-";
        }else if(i==12){
            return "/";
        }else if(i==13){
            return "*";
        }else if(i==14){
            return "^";
        }else if(i==15){
            return "(";
        }else if(i==16){
            return ")";
        }else if(i==17){
            return ".";
        }else if(i==18){
            return "E";
        }else if(i==19){
            return "DEL";
        }else if(i==20){
            return "EXEC";
        }else{
            return "AC";
        }
    }

    /**
     * converts the strings to integers
     * @param s the string to convert
     * @return the integer that the string represented.
     */
    public static int stringToInt(String s){ //used to convert the strings to integers.
        switch(s){
            case "0":
                return 0;
            case "1":
                return 1;
            case "2":
                return 2;
            case "3":
                return 3;
            case "4":
                return 4;
            case "5":
                return 5;
            case "6":
                return 6;
            case "7":
                return 7;
            case "8":
                return 8;
            case "9":
                return 9;
            case "+":
                return 10;
            case "-":
                return 11;
            case "/":
                return 12;
            case "*":
                return 13;
            case "^":
                return 14;
            case "(":
                return 15;
            case ")":
                return 16;
            case ".":
                return 17;
            case "E":
                return 18;
            case "DEL":
                return 19;
            case "EXEC":
                return 20;
            case "AC":
                return 21;
            default:
                throw new IllegalArgumentException("Can't process " + s);
        }
    }




}
