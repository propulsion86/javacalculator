package source;
import java.util.ArrayList;
import java.util.List;

public class CommonData {
    private List<String> input = new ArrayList<>(); //the input that the evaluate expression method will use.

    private boolean update = false; //allows other classes cause the output to update.

    public CommonData(){}

    /**
     * returns the string represented by the strings in the list.
     * @return the String that the list represents.
     */

    public String getToOut(){ //returns the string represented by the input list.
        String toReturn = "";
        for(String s : input){
            toReturn = toReturn + s;
        }
        return toReturn;
    }

    /**
     * adds a string to the input list
     * @param s the string to add.
     */
    public void addInput(String s){ //add a string to the input list.
        input.add(s);
    } //adds a string to the input list.

    /**
     * returns the input list.
     * @return the input list
     */
    public List<String> getInput(){ //get the input list.
        return input;
    } //returns the input list.

    /**
     * deletes all strings in the input list.
     */
    public void clearInput(){input.clear();} //delete all strings in the input list.

    /**
     * deletes the last string in the input list.
     */
    public void delInputEnd(){ //delete the last character in the input.
        input.remove(input.size()-1);
    } //delete the last character in the input.

    /**
     * set the update variable to true
     */
    public void setUpdate(){ //set the update variable to true.
        update = true;
    } //set the update variable to true.

    /**
     * set the update variable to false
     */
    public void unSetUpdate(){ //set the update variable to false.
        update = false;
    }

    /**
     * return the update variable's state.
     * @return the state of the update varible.
     */
    public Boolean getUpdate(){ //return the update variable's state.
        return update;
    }



}
