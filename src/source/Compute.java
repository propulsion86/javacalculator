package source;
import java.util.List;
import java.util.Stack;

public class Compute {
    public Compute(){}

    /**
     * The function called to evaluate an entire equation
     * @param equation the list of strings that hold the operands and operators.
     * @return the answer.
     */
    public static double evaluateEquation(List<String> equation) { //is called where EXEC is pressed.
        Stack<Double> operandStack = new Stack<>();
        Stack<String> operatorStack = new Stack<>();

        for (int i=0; i < equation.size(); i++) {
            String token = equation.get(i); //get the token so that the checks can be performed.
            if (isNumeric(token)) { // if token is a number
                Double temp = Double.parseDouble(token); //parse it
                boolean isDec = false; //declare and initialise isDec. true if temp is found to be a decimal.
                if(i!=equation.size()-1){ //check if it is the last character.
                    if(!isOperator(equation.get(i+1))){ //make sure that it is not an operator.
                        int count = 1; //declare and initialise count. used to put the decimal characters in the right place.
                        while(!isOperator(equation.get(i+1))){ //while not an operator.
                            if(equation.get(i+1).equals(".")){isDec = true; i++;}

                            if(isDec){
                                if(equation.get(i+1).equals(")")){break;} //check for brackets.
                                Integer intHold = Integer.valueOf(equation.get(i+1)); //holds an integer value to be put into the correct decimal place.
                                Double place = Math.pow(10, count); // the power of 10 to divide by. E.g. number is 0.12 and a 3 is next. count = 3. so place = 1000. decAdd = 3/1000 = 0.003. then temp = temp + 0.003 = 0.123.
                                Double decAdd = intHold / place;
                                temp = temp + decAdd;
                                count++; //increase count
                                i++ ; //move i forward 1.
                            }else{ // if normal number.
                                if(equation.get(i+1).equals(")")){break;} //check for brackets.
                                Integer toAdd = Integer.valueOf(equation.get(i+1)); //get the integer value.
                                temp = temp*10; //move temp over one decimal place.
                                temp = temp + toAdd; //add the new integer to the end.
                                i++;
                            }
                            if(i >= equation.size()-1){break;} //check if i is the size of the list.

                        }

                    }
                }
                operandStack.push(temp);
            } else if (token.equals("(")) {
                operatorStack.push(token);
            } else if (token.equals(")")) {
                while (!operatorStack.isEmpty() && !operatorStack.peek().equals("(")) {
                    performOperation(operandStack, operatorStack);
                }
                operatorStack.pop(); // Remove the open bracket
            } else if (isOperator(token)) {
                while (!operatorStack.isEmpty() && hasPrecedence(token, operatorStack.peek())) {
                    performOperation(operandStack, operatorStack);
                }
                operatorStack.push(token);
            }
        }

        while (!operatorStack.isEmpty()) {
            performOperation(operandStack, operatorStack);
        }

        return operandStack.pop();
    }

    /**
     * checks if the string is a number.
     * @param str the string to check
     * @return true or false.
     */
    private static boolean isNumeric(String str) { //checks if the string is a number
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    /**
     * Checks if the string is an operator.
     * @param str the string to check
     * @return true or false.
     */
    private static boolean isOperator(String str) { //checks if the string is an operator.
        return "+-*/^E".contains(str);
    }

    /**
     * assings a precence ranking according to bedmas.
     * @param operator the string
     * @return the ranking from 0 to 4
     */
    private static int getPrecedence(String operator) { //controls order of operations (bedmas)
        switch (operator) {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            case "^":
                return 3;
            case "E":
                return 4;
            default:
                return 0; // Lower precedence for other characters
        }
    }

    /**
     * Acturlly checks the precedence.
     * @param op1 the first operator
     * @param op2 the second operator
     * @return true or false.
     */
    private static boolean hasPrecedence(String op1, String op2) {// does the order of operations checking.
        return getPrecedence(op1) <= getPrecedence(op2);
    }

    /**
     * does the operations one at a time
     * @param operandStack the operands
     * @param operatorStack the operators.
     */
    private static void performOperation(Stack<Double> operandStack, Stack<String> operatorStack) { //gets operands and operator of the stack and then call applyOperation on them.
        String operator = operatorStack.pop();
        double operand2 = operandStack.pop();
        double operand1 = operandStack.pop();

        System.out.println(operand1 + " " + operator + " " + operand2);

        double result = applyOperator(operand1, operand2, operator);
        operandStack.push(result);
    }

    /**
     * actually does the individual actions such as +, -, /, *, ^, E
     * @param operand1 first operand
     * @param operand2 second operand
     * @param operator the operator
     * @return the result.
     */
    private static double applyOperator(double operand1, double operand2, String operator) { //Does the operation.
        switch (operator) {
            case "+":
                return operand1 + operand2;
            case "-":
                return operand1 - operand2;
            case "*":
                return operand1 * operand2;
            case "/":
                if (operand2 != 0) {
                    return operand1 / operand2;
                } else {
                    throw new ArithmeticException("Division by zero");
                }
            case "^":
                return Math.pow(operand1, operand2);
            case "E": //"E" is x 10 ^ n. E.g. 2E4 = 2 * (10 ^ 4) = 20000
                return operand1 * Math.pow(10, operand2);
            default:
                throw new IllegalArgumentException("Invalid operator: " + operator);
        }
    }

}
