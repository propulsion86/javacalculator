# **JavaCalculator**


## Purpose 

This program was made to practice making applications with GUIs.

## What can it do
Can do Addition, Subtraction, Division, Multiplication.

Can handle "E", Powers and Brackets.

Equations are evaluated using BEDMAS.

## Installation Instruction

### Dependencies

openjdk >= 21.0.0

### Arch Linux
```
sudo pacman -S jdk-openjdk git 
git clone https://gitlab.com/propulsion86/javacalculator.git
cd javacalculator/src/source
javac *.javagit clone https://gitlab.com/propulsion86/javacalculator.git
java Main
```

### Ubuntu Linux
```
sudo apt install openjdk-21-jdk
git clone https://gitlab.com/propulsion86/javacalculator.git
cd javacalculator/src/source
javac *.java
java Main
```

### Fedora Linux
```
sudo dnf install java-latest-openjdk
git clone https://gitlab.com/propulsion86/javacalculator.git
cd javacalculator/src/source
javac *.java
java Main
```

### Windows
[Install Openjdk 21](https://aka.ms/download-jdk/microsoft-jdk-21.0.1-windows-x64.msi)

Open Command Line or PowerShell Session

```
cd C:\user\<your-username>
git clone https://gitlab.com/propulsion86/javacalculator.git
cd javacalculator\src\source
java *.java
javac Main
```

### Mac
[Install brew](https://brew.sh/)

Open terminal

Install openjdk with brew

'brew install openjdk'

```
cd ~/
git clone https://gitlab.com/propulsion86/javacalculator.git
cd javacalculator/src/source
java *.java
javac Main
```

